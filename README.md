# ProcessWire Textformatter Module for Inlay shortcode support

## What is Inlay?

It’s a way to configure custom forms and data presentations from within
CiviCRM that can then be added to external websites.

See the [CiviCRM Inlay extension](https://lab.civicrm.org/extensions/inlay)


## What is this module?

Inlay generates `<script>` tags, but these might be hard to put in the
right place, so this provides a WordPress-style shortcode.

These look like `[inlay id="abcdef012345"]`

Once you have this module set-up, you can copy and paste shortcodes like that 
into text fields on your site to control placement of Inlays.

## How to set it up

1. Install this module in ProcessWire and configure it by supplying the base URL to where Inlay's scripts are stored. e.g. if Inlay's script tags look like this:

   ```
   <script src="https://example.org/sites/default/files/civicrm/inlay-abcdef012345.js" data-inlay-id="abcdef012345" ></script>
   ```

   Then the base URL would be `https://example.org/sites/default/files/civicrm/`

2. Edit the field(s) you wish to use it on, and under their Details tab, where you can select Text formatters, add **Inlay shortcode support**.

That’s it.

This is the ProcessWire version of a similar [WordPress plugin](https://github.com/artfulrobot/inlay-wp/blob/main/inlay.php).

